Secondary disk
==============

Very simple role that formats a secondary disk and mounts it to a specified directory.
This is not intended to be used in production, but rather as a quick way to add a secondary disk to a VM.

Requirements
------------

The machine should have a secondary disk attached to it.

Role Variables
--------------

`device` - The device to format and mount. Defaults to `/dev/sdb`.
`fs_type` - The filesystem type to use. Defaults to `xfs`.
`mount_path` - The path to mount the disk to. Defaults to `/data`.

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: secondary_disk, device: /dev/sdb, fs_type: xfs, mount_path: /srv }

License
-------

BSD

Author Information
------------------

Emmanuel Jaep
